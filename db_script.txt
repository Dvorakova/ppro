CREATE DATABASE  IF NOT EXISTS `db_ppro` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `db_ppro`;
-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: db_ppro
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regNumber` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `phone` int(11) NOT NULL,
  `contactPerson` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `regNumber_UNIQUE` (`regNumber`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (2,123456789,'Zuzana Vaňousová','Seč 47',774141572,'Zuzana Vaňousová'),(4,987654321,'Karel Podnik','Kostelní 15, 12345 Praha',775552980,'Karel Pepa'),(16,456789123,'Basileios Druhý','Záchlumí',739112548,'Basin'),(17,123,'Common','COMMON',1,'COMMON'),(18,48752596,'Dosedla Trans','Postoloprty 15',732616584,'Jan Dosedla'),(19,951753,'AKADEMIE','Dolní Libchavy 157',456123741,'Denisa Vzdělaná'),(20,852456159,'Verbal Publishing','Československá 45',654321987,'Strejda Karel'),(21,741852963,'Ministarstvo dopravy','nábřeží Ludvíka Svobody 1222/12',225131111,'Vladimír Kremlík'),(22,963741825,'DFens','Karolíny Světlé 85',465629222,'Petr Jaroš');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `job` enum('Accountant','Assistant','Lecturer') NOT NULL,
  `login` varchar(45) NOT NULL,
  `password` varchar(455) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idEmployee_UNIQUE` (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'Marie','Kudeříková','Accountant','markud','$2y$12$3VLSil06OMYirfsWHFr7pOC5FAVsKjWY1sshJ13YO6afPbk7q4X1u'),(2,'Anna','Polívková','Assistant','annpol','$2y$12$jrjtmuZ.j/vL/T.87kxjK.36jP7I8RPvMv2/9qXgBbdSCgzYpkFVW'),(3,'David','Krejčí','Assistant','davkre','$2y$12$GUFHNed43U9QJt9jsWoCHOBcUi8ZWxVQlBb94kslrVhS3m3Eb9.YO'),(4,'Josef','Zmrhal','Lecturer','joszmr','$2y$12$vJre2XRYqFbP6TCkKBBCQ.2qPy68NNtF7MbAKtTCG7yXFJYrNRYOO'),(5,'Roman','Kefalín','Lecturer','romkef','$2y$12$I4xhz6RRtWc8TLAoWhFfqOFIs8PsQlTQo3f3AKH8WX6TSqX9NkLp2'),(6,'Zuzana','Čaputová','Lecturer','zuzcap','$2y$12$2b85wUv2RnrOjEOdmpI3COvR96iet9qF.hETsyeKIb0axD/l9E5pm'),(7,'Bára','Hrzánová','Lecturer','barhrz','$2y$12$yY1OgMhOlYLutoX9xUDKR.TysxpGthjgCz0rS4ZHFB4T7hasFdgTi'),(9,'Zuzana','Vaňousová','Accountant','zuzvan','$2y$12$IGjoQF/fGPArMNmE8RxQZOUXff1lE287etVe9KJDXDjO/KRT5Q4PC'),(10,'Martin','Dvořák','Lecturer','mardvo','$2a$10$DMCdYlqjYd.MrfCxZauZ5.SXoHBPL1eE7vEJjpTZOhJqxmukjh3X6');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `capacity` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idRoom_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` VALUES (1,15,'Red'),(2,20,'Green'),(3,15,'Blue'),(4,15,'Purple'),(8,30,'Big Apple'),(9,10,'Sardines'),(10,15,'White');
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seminar`
--

DROP TABLE IF EXISTS `seminar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seminar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('Private','Common') NOT NULL,
  `numberOfAtendees` int(11) NOT NULL,
  `idRoom` int(11) NOT NULL,
  `idAssistant` int(11) NOT NULL,
  `idLecturer` int(11) NOT NULL,
  `idCustomer` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idSeminar_UNIQUE` (`id`),
  KEY `idEmployee_idx` (`idAssistant`,`idLecturer`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seminar`
--

LOCK TABLES `seminar` WRITE;
/*!40000 ALTER TABLE `seminar` DISABLE KEYS */;
INSERT INTO `seminar` VALUES (1,'Common',15,1,2,4,17),(2,'Private',18,4,2,4,2),(3,'Private',16,2,3,5,2),(5,'Common',15,1,2,4,17),(6,'Common',16,8,3,7,17),(7,'Private',13,10,3,7,16),(8,'Common',30,8,2,7,17),(9,'Private',8,9,2,10,19),(10,'Private',12,2,2,6,22),(11,'Private',11,4,2,10,21),(12,'Private',9,10,3,5,4),(13,'Private',29,8,3,7,20),(14,'Private',16,2,3,6,18);
/*!40000 ALTER TABLE `seminar` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-28 11:17:59
