package uhk.vanousova.PPRO;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PproApplication {

	public static void main(String[] args) {
		SpringApplication.run(PproApplication.class, args);
	}

}
