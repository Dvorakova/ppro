package uhk.vanousova.PPRO.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import uhk.vanousova.PPRO.model.Customer;
import uhk.vanousova.PPRO.repository.CustomerRepository;


@Controller
@RequestMapping("/customers/")
public class CustomerController {
	
	@Autowired
	private CustomerRepository customerRepository;

	
	@GetMapping("all")
    public String showUpdateForm(Model model) {
        model.addAttribute("customers", customerRepository.findAll());
        return "all-customers";
    }
	
	@GetMapping("addcustomer")
    public String showSignUpForm(Customer customer) {
        return "add-customer";
    }
	
	 @PostMapping("add")
	    public String addCustomer(@Valid Customer customer, BindingResult result, Model model) {
		 if (result.hasErrors()) {
	            return "add-customer";
	        }   
		 try {customerRepository.save(customer);
				
			} catch (DataIntegrityViolationException e) {
				model.addAttribute("errorMessage", "There already exist a customer with inserted registration number. Check the registration number or update existing customer via update form.");
				return "error-message";
			}
	        
	        return "redirect:all";
	    }
	 
	 @GetMapping("edit/{id}")
	    public String showUpdateForm(@PathVariable("id") int id, Model model) {
		 Customer customer = customerRepository.findById(id)
	            .orElseThrow(() -> new IllegalArgumentException("Invalid customer Id:" + id));
	        model.addAttribute("customer", customer);
	        return "update-customer";
	    }
	 
	 @PostMapping("update/{id}")
	    public String updateCustomer(@PathVariable("id") int id, @Valid Customer customer, BindingResult result, Model model) {
	        if (result.hasErrors()) {
	        	customer.setId(id);
	            return "update-customer";
	        }
	        try {customerRepository.save(customer);
	        model.addAttribute("customers", customerRepository.findAll());
	        return "all-customers";
				
			} catch (DataIntegrityViolationException e) {
				model.addAttribute("errorMessage", "There already exist a customer with inserted registration number. Check the registration number or update existing customer via update form.");
				return "error-message";
			}
	        
	    }
	 
	 @GetMapping("delete/{id}")
	    public String deleteCustomer(@PathVariable("id") int id, Model model) {
		 	Customer customer = customerRepository.findById(id)
	            .orElseThrow(() -> new IllegalArgumentException("Invalid customer Id:" + id));
	        customerRepository.delete(customer);
	        model.addAttribute("customers", customerRepository.findAll());
	        return "all-customers";
	    }
}
