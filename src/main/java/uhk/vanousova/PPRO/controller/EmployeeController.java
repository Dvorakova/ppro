package uhk.vanousova.PPRO.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import uhk.vanousova.PPRO.model.Employee;
import uhk.vanousova.PPRO.repository.EmployeeRepository;
import uhk.vanousova.PPRO.service.EmployeeService;


@Controller
@RequestMapping("/employees/")
public class EmployeeController {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired 
	private EmployeeService employeeService;

	
	@GetMapping("all")
    public String showUpdateForm(Model model) {
        model.addAttribute("employees", employeeRepository.findAll());
        return "all-employees";
    }
	
	@GetMapping("addemployee")
    public String showSignUpForm(Employee employee) {
        return "add-employee";
    }
	
	 @PostMapping("add")
	    public String addEmployee(@Valid Employee employee, BindingResult result, Model model) {
		 if (result.hasErrors()) {
	            return "add-employee";
	        }   
	        try {
	        	employeeService.saveEmployee(employee);
		        return "redirect:all";
			} catch (DataIntegrityViolationException e) {
				model.addAttribute("errorMessage", "There already exist employee with inserted login. Check the login or update existing employee via update form.");
				return "error-message";
			}
	        
	    }
	 
	 @GetMapping("edit/{id}")
	    public String showUpdateForm(@PathVariable("id") int id, Model model) {
	        Employee employee = employeeRepository.findById(id)
	            .orElseThrow(() -> new IllegalArgumentException("Invalid emyployee Id:" + id));
	        model.addAttribute("employee", employee);
	        return "update-employee";
	    }
	 
	 @PostMapping("update/{id}")
	    public String updateEmployee(@PathVariable("id") int id, @Valid Employee employee, BindingResult result, Model model) {
		 if (result.hasErrors()) {
	        	employee.setId(id);
	            return "update-employee";
	        }  
		 try {
	        	employeeService.saveEmployee(employee);
		        model.addAttribute("employees", employeeRepository.findAll());
		        return "all-employees";
				
			} catch (DataIntegrityViolationException e) {
				model.addAttribute("errorMessage", "There already exist employee with inserted login. Check the login or update existing employee via update form.");
				return "error-message";
			}

	    }
	 
	 @GetMapping("delete/{id}")
	    public String deleteEmployee(@PathVariable("id") int id, Model model) {
	        Employee employee = employeeRepository.findById(id)
	            .orElseThrow(() -> new IllegalArgumentException("Invalid employee Id:" + id));
	        employeeRepository.delete(employee);
	        model.addAttribute("employees", employeeRepository.findAll());
	        return "all-employees";
	    }
}
