package uhk.vanousova.PPRO.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import uhk.vanousova.PPRO.model.Employee;
import uhk.vanousova.PPRO.repository.EmployeeRepository;
import uhk.vanousova.PPRO.repository.SeminarRepository;

@Controller
@RequestMapping(path="/")
public class MainController {
	
	
	@Autowired
	private SeminarRepository seminarRepository;
	@Autowired
	private EmployeeRepository employeeRepository;
	
	
	
	
	@GetMapping({"/","/login"})
	public String showLoginForm(Model model) {
		return "login";
	}
	
	@RequestMapping("/default")
	public String defaultAfterLogin() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		String login = auth.getName();
		
		Employee current = employeeRepository.findByLogin(login);
		
		switch (current.getJob()) {
		case "Accountant":
			return "redirect:/seminars/all";
		case "Lecturer":
			return "redirect:/show";
		case "Assistant":
			return "redirect:/seminars/all";
		default:
			return "/";
		}
	}
	
	@GetMapping("show")
	 public String showSeminarsByEmployeeId(Model model) {
		 Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			
			String login = auth.getName();
			
			Employee current = employeeRepository.findByLogin(login);
	
		 model.addAttribute("lecturerSeminars", seminarRepository.findByLecturer(current));
		 return "show-seminars";
	 }
	
	@GetMapping("access-denied")
	public String showAccessDenied(Model model) {
		return "access-denied";
	}
	
	@GetMapping("error")
	public String showError(Model model) {
		return "error";
	}
	
	
	
	
	
	

}
