package uhk.vanousova.PPRO.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import uhk.vanousova.PPRO.model.Room;
import uhk.vanousova.PPRO.repository.RoomRepository;


@Controller
@RequestMapping("/rooms/")
public class RoomController {
	
	@Autowired
	private RoomRepository roomRepository;

	
	@GetMapping("all")
    public String showUpdateForm(Model model) {
        model.addAttribute("rooms", roomRepository.findAll());
        return "all-rooms";
    }
	
	@GetMapping("addroom")
    public String showSignUpForm(Room room) {
        return "add-room";
    }
	
	 @PostMapping("add")
	    public String addRoom(@Valid Room room, BindingResult result, Model model) {
		 if (result.hasErrors()) {
	            return "add-room";
	        }
		 try {roomRepository.save(room);
		 		return "redirect:all";
			
			} catch (DataIntegrityViolationException e) {
				model.addAttribute("errorMessage", "There already exist room with inserted name. Check the name or update existing room via update form.");
				return "error-message";
			}
	        
	    }
	 
	 @GetMapping("edit/{id}")
	    public String showUpdateForm(@PathVariable("id") int id, Model model) {
	        Room room = roomRepository.findById(id)
	            .orElseThrow(() -> new IllegalArgumentException("Invalid room Id:" + id));
	        model.addAttribute("room", room);
	        return "update-room";
	    }
	 
	 @PostMapping("update/{id}")
	    public String updateRoom(@PathVariable("id") int id, @Valid Room room, BindingResult result, Model model) {
		 if (result.hasErrors()) {
	        	room.setId(id);
	            return "update-room";
	        }
		 try {roomRepository.save(room);
		 	  model.addAttribute("rooms", roomRepository.findAll());
	          return "all-rooms";
			
			} catch (DataIntegrityViolationException e) {
				model.addAttribute("errorMessage", "There already exist room with inserted name. Check the name or update existing room via update form.");
				return "error-message";
			}

		    
	    }
	 
	 @GetMapping("delete/{id}")
	    public String deleteRoom(@PathVariable("id") int id, Model model) {
	        Room room = roomRepository.findById(id)
	            .orElseThrow(() -> new IllegalArgumentException("Invalid room Id:" + id));
	        roomRepository.delete(room);
	        model.addAttribute("rooms", roomRepository.findAll());
	        return "all-rooms";
	    }
}
