package uhk.vanousova.PPRO.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import uhk.vanousova.PPRO.model.Customer;
import uhk.vanousova.PPRO.model.Employee;
import uhk.vanousova.PPRO.model.Room;
import uhk.vanousova.PPRO.model.Seminar;
import uhk.vanousova.PPRO.repository.CustomerRepository;
import uhk.vanousova.PPRO.repository.EmployeeRepository;
import uhk.vanousova.PPRO.repository.RoomRepository;
import uhk.vanousova.PPRO.repository.SeminarRepository;


@Controller
@RequestMapping("/seminars/")
public class SeminarController {
	

	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private RoomRepository roomRepository;
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private SeminarRepository seminarRepository;

	@ModelAttribute("rooms")
	public Iterable<Room> getRooms() {
		return roomRepository.findAll();
	}
	
	@ModelAttribute("customers")
	public Iterable<Customer> getCustomers() {
		return customerRepository.findAll();
	}
	
	@ModelAttribute("lecturers")
	public Iterable<Employee> getLecturers() {
		return employeeRepository.findByJob("Lecturer");
	}
	
	@ModelAttribute("assistants")
	public Iterable<Employee> getAssistants() {
		return employeeRepository.findByJob("Assistant");
	}
	
	@ModelAttribute("seminars")
	public Iterable<Seminar> getSeminars() {
		return seminarRepository.findAll();
	}
	
	@GetMapping("all")
    public String showUpdateForm(Model model) {
        model.addAttribute("seminars", seminarRepository.findAll());
        return "all-seminars";
    }
	
	@GetMapping("addseminar")
    public String showSignUpForm(Seminar seminar) {
        return "add-seminar";
    }
	
	 @PostMapping("add")
	    public String addSeminar(@Valid Seminar seminar, BindingResult result, Model model) {
		 if (result.hasErrors()) {
			 
	            return "add-seminar";
	        }
		 Room wantedRoom = seminar.getRoom();
		 if (seminar.getNumberOfAtendees() > wantedRoom.getCapacity()) {
			 model.addAttribute("errorMessage", "Selected room is not big enough. Selected bigger room for this seminar.");
				return "error-message";
		 }
		 
		 String seminarType = seminar.getType();
		 
		 if ((seminarType.equals("Common")) && !(seminar.getCustomer().getName().equals("Common"))){
			 model.addAttribute("errorMessage", "Please select customer Common for nonprivate seminars.");
				return "error-message";		 
		 }
		 
        	seminarRepository.save(seminar);
	        return "redirect:all";

	    }
	 
	 @GetMapping("edit/{id}")
	    public String showUpdateForm(@PathVariable("id") int id, Model model) {
		 Seminar seminar = seminarRepository.findById(id)
	            .orElseThrow(() -> new IllegalArgumentException("Invalid seminar Id:" + id));
	        model.addAttribute("seminar", seminar);
	        return "update-seminar";
	    }
	 
	 @PostMapping("update/{id}")
	    public String updateSeminar(@PathVariable("id") int id, @Valid Seminar seminar, BindingResult result, Model model) {
	        if (result.hasErrors()) {
	        	seminar.setId(id);
	            return "update-seminar";
	        }
	        Room wantedRoom = seminar.getRoom();
			 if (seminar.getNumberOfAtendees() > wantedRoom.getCapacity()) {
				 model.addAttribute("errorMessage", "Selected room is not big enough. Selected bigger room for this seminar.");
					return "error-message";
			 }
			 
			 String seminarType = seminar.getType();
			 
			 if ((seminarType.equals("Common")) && !(seminar.getCustomer().getName().equals("Common"))){
				 model.addAttribute("errorMessage", "Please select customer Common for nonprivate seminars.");
					return "error-message";		 
			 }
			 
	        seminarRepository.save(seminar);
	        model.addAttribute("seminars", seminarRepository.findAll());
	        return "all-seminars";
	    }
	 
	 @GetMapping("delete/{id}")
	    public String deleteSeminar(@PathVariable("id") int id, Model model) {
		 	Seminar seminar = seminarRepository.findById(id)
	            .orElseThrow(() -> new IllegalArgumentException("Invalid seminar Id:" + id));
	        seminarRepository.delete(seminar);
	        model.addAttribute("seminars", seminarRepository.findAll());
	        return "all-seminars";
	    }
	 
	 
}
