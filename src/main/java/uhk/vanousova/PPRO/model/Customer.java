package uhk.vanousova.PPRO.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;




@Entity
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull(message= "Registration number must be entered")
	@Column(unique=true)
	private Integer regNumber;
	
	@NotBlank(message= "Name cannot be empty")
	private String name;
	
	@NotBlank(message= "Adress cannot be empty")
	private String address;
	
	@NotNull(message= "Phone must be entered")
	private Integer phone;
	
	@NotBlank(message= "There must be a contact person indetified")
	private String contactPerson;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(Integer regNumber) {
		this.regNumber = regNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public Customer(Integer regNumber, String name, String address, Integer phone, String contactPerson) {
		super();
		this.regNumber = regNumber;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.contactPerson = contactPerson;
	}
	
	public Customer() {
		
	}
	
	
	
}
