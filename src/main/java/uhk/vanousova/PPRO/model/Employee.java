package uhk.vanousova.PPRO.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotBlank(message= "First name cannot be empty")
	private String firstName;
	
	@NotBlank(message= "Last name cannot be empty")
	private String lastName;
	
	@Column(name="job", columnDefinition="enum('Assistant','Accountant','Lecturer')")
	private String job;

	@NotBlank(message= "Login cannot be empty")
	@Column(unique=true)
	private String login;
	
	@NotBlank(message= "Password cannot be empty")
	private String password;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Employee(String firstName, String lastName, String job) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.job = job;
	}
	
	public Employee(String firstName, String lastName, String job, String login, String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.job = job;
		this.login = login;
		this.password = password;
	}

	public Employee() {
		
	}

	
	
}
