package uhk.vanousova.PPRO.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Seminar{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="type", columnDefinition="enum('Private','Common')")
	private String type;
	
	@NotNull(message="Number of attendees cannot be 0")
	private Integer numberOfAtendees;
	
	@ManyToOne
	@JoinColumn(name="idRoom", nullable=false)
	private Room room;
	
	@ManyToOne
	@JoinColumn(name="idAssistant", nullable=false)
	private Employee assistant;
	
	@ManyToOne
	@JoinColumn(name="idLecturer", nullable=false)
	private Employee lecturer;
	
	
	@ManyToOne
	@JoinColumn(name="idCustomer")
	private Customer customer;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Integer getNumberOfAtendees() {
		return numberOfAtendees;
	}


	public void setNumberOfAtendees(Integer numberOfAtendees) {
		this.numberOfAtendees = numberOfAtendees;
	}


	public Room getRoom() {
		return room;
	}


	public void setRoom(Room room) {
		this.room = room;
	}


	public Employee getAssistant() {
		return assistant;
	}


	public void setAssistant(Employee assistant) {
		this.assistant = assistant;
	}


	public Employee getLecturer() {
		return lecturer;
	}


	public void setLecturer(Employee lecturer) {
		this.lecturer = lecturer;
	}


	public Customer getCustomer() {
		return customer;
	}


	public void setCustomer(Customer customer) {
		this.customer = customer;
	}


	public Seminar(String type, Integer numberOfAtendees, Room room, Employee assistant, Employee lecturer,
			Customer customer) {
		super();
		this.type = type;
		this.numberOfAtendees = numberOfAtendees;
		this.room = room;
		this.assistant = assistant;
		this.lecturer = lecturer;
		this.customer = customer;
	}
	
	public Seminar () { }
	
}
