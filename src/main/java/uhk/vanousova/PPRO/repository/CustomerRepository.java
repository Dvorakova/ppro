package uhk.vanousova.PPRO.repository;

import org.springframework.data.repository.CrudRepository;

import uhk.vanousova.PPRO.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer>{

}
