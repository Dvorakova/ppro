package uhk.vanousova.PPRO.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import uhk.vanousova.PPRO.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer>{
	List<Employee> findByJob(String job);
	Employee findByLogin(String login);
	

}
