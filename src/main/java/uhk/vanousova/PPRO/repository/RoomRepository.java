package uhk.vanousova.PPRO.repository;

import java.util.List;


import org.springframework.data.repository.*;

import uhk.vanousova.PPRO.model.Room;

public interface RoomRepository extends CrudRepository<Room, Integer> {

	List<Room> findByName(String name);
	
	
}
