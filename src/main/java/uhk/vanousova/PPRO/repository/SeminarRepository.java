package uhk.vanousova.PPRO.repository;

import java.util.List;

import org.springframework.data.repository.*;

import uhk.vanousova.PPRO.model.Employee;
import uhk.vanousova.PPRO.model.Seminar;

public interface SeminarRepository extends CrudRepository<Seminar, Integer> {

	List<Seminar> findByLecturer(Employee lecturer);
	
}
