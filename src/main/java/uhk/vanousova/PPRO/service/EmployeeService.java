package uhk.vanousova.PPRO.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import uhk.vanousova.PPRO.model.*;
import uhk.vanousova.PPRO.repository.EmployeeRepository;

@Service
public class EmployeeService implements UserDetailsService {
	
	private EmployeeRepository employeeRepository;

	
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	public EmployeeService(EmployeeRepository employeeRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.employeeRepository = employeeRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}
	
	public EmployeeService() {
		this.bCryptPasswordEncoder = new BCryptPasswordEncoder();
		
	}

	public Employee findEmployeeByLogin(String login) {
		return employeeRepository.findByLogin(login);
	}
	
	public Employee saveEmployee(Employee employee) {
		employee.setPassword(bCryptPasswordEncoder.encode(employee.getPassword()));
		return employeeRepository.save(employee);
	}
	
	private List<GrantedAuthority> getEmployeeAuthority(Employee employee) {
		Set<GrantedAuthority> role = new HashSet<GrantedAuthority>();
		role.add(new SimpleGrantedAuthority(employee.getJob()));
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>(role);
        return grantedAuthorities;
	}
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String login) {
		
		Employee employee = this.findEmployeeByLogin(login);
		
		if (employee == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
		
		return new org.springframework.security.core.userdetails.User(employee.getLogin(), employee.getPassword(), this.getEmployeeAuthority(employee) );
		
	}
	
	
}
