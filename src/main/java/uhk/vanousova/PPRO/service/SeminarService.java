package uhk.vanousova.PPRO.service;

import java.util.List;

import org.springframework.stereotype.Service;

import uhk.vanousova.PPRO.model.Seminar;
import uhk.vanousova.PPRO.repository.*;
import uhk.vanousova.PPRO.model.Employee;;

@Service
public class SeminarService {
	
	private SeminarRepository seminarRepository;

	
	public List<Seminar> getAllLecturerSeminars(Employee lecturer) {
		return seminarRepository.findByLecturer(lecturer);
	}

	public SeminarService() {
		
	}
}
