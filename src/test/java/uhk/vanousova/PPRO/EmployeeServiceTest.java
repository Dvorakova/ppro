package uhk.vanousova.PPRO;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import uhk.vanousova.PPRO.model.Employee;
import uhk.vanousova.PPRO.repository.EmployeeRepository;
import uhk.vanousova.PPRO.service.EmployeeService;

@SpringBootTest
public class EmployeeServiceTest {
	
	@Mock
	private EmployeeRepository employeeRepository;
		
	@InjectMocks
    private EmployeeService employeeService = new EmployeeService();
	
	@BeforeEach
	public void setUp() {
		Employee testEmployee = new Employee("Test", "Test", "Assistant", "testLogin", "testpassword");
		
		when(employeeRepository.findByLogin(testEmployee.getLogin())).thenReturn(testEmployee);
	}
	
	@Test
	public void whenValidName_thenEmployeeShouldBeFound() {
	    String login = "testLogin";
	    Employee found = employeeService.findEmployeeByLogin(login);
	  
	     assertThat(found.getLogin()).isEqualTo(login);
	 }
	
	@Test()
	public void whenInvalidName_thenNull() {
		String login = "falseTestLogin";
		assertNull(employeeService.findEmployeeByLogin(login));
	}
	
	@Test()
	public void whenSave_thenPasswordShouldBeHashed() {
		Employee testEmployee = new Employee("Test", "Test", "Assistant", "testLogin", "testpassword");
		employeeService.saveEmployee(testEmployee);
		
		assertNotEquals(testEmployee.getPassword(), "testpassword");
	}
   

}
