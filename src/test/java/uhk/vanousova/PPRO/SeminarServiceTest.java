package uhk.vanousova.PPRO;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import uhk.vanousova.PPRO.model.Customer;
import uhk.vanousova.PPRO.model.Employee;
import uhk.vanousova.PPRO.model.Room;
import uhk.vanousova.PPRO.model.Seminar;
import uhk.vanousova.PPRO.repository.EmployeeRepository;
import uhk.vanousova.PPRO.repository.SeminarRepository;
import uhk.vanousova.PPRO.service.SeminarService;

@SpringBootTest
public class SeminarServiceTest {
	
	@Mock
	private SeminarRepository seminarRepository;
	
	@Mock
	private EmployeeRepository employeeRepository;
		
	@InjectMocks
    private SeminarService seminarService = new SeminarService();
	

	@Test
	public void WhenValidLecturer_SeminarsShouldBeFound() {
		Employee testLecturer = new Employee("Test", "Test", "Lecturer", "testLogin", "testpassword");
		Employee testAssistant = new Employee("Test", "Test", "Assistant", "testLoginAssistant", "testpassword");
		Customer testCustomer = new Customer(999, "name", "address", 12345678, "contactPerson");
		Room testRoom = new Room("name",100);
		Seminar testSeminar = new Seminar("Private", 1, testRoom, testAssistant, testLecturer, testCustomer);
		
		when(seminarService.getAllLecturerSeminars(testLecturer)).thenReturn(asList(testSeminar));
		
		List<Seminar> actual = seminarService.getAllLecturerSeminars(testLecturer);
		
		assertEquals(testSeminar, actual.get(0));
		}
	
	@Test
	public void WhenInvalidLecturer_SeminarsShouldBeFound() {
		Employee testLecturer = new Employee("Test", "Test", "Lecturer", "testLogin", "testpassword");
		Employee testAssistant = new Employee("Test", "Test", "Assistant", "testLoginAssistant", "testpassword");
		Customer testCustomer = new Customer(999, "name", "address", 12345678, "contactPerson");
		Room testRoom = new Room("name",100);
		Seminar testSeminar = new Seminar("Private", 1, testRoom, testAssistant, testLecturer, testCustomer);
		
		Employee testInvalidLecturer = new Employee("InTest", "InTest", "Lecturer", "inTestLogin", "testpassword");
		
		when(seminarService.getAllLecturerSeminars(testLecturer)).thenReturn(asList(testSeminar));
		
		assertThat(seminarService.getAllLecturerSeminars(testInvalidLecturer).isEmpty() );
	}
		
		
}
